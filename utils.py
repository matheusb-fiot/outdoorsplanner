from datetime import datetime, timedelta

# How many days we will present google calendar events and weather forecasts
# to the user.
#
# NOTE: make sure your forecast API accepts at least this many days of hourly
# data.
VIEW_DAYS=3

# From https://stackoverflow.com/a/48938464/11019779 (CC BY-SA 3.0)
# by Anton vBR (https://stackoverflow.com/users/7386332/anton-vbr)
def hour_rounder(t):
    # Rounds to nearest hour by adding a timedelta hour if minute >= 30
    return (t.replace(second=0, microsecond=0, minute=0, hour=t.hour) +
            timedelta(hours=t.minute//30))
