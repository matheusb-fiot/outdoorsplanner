#
# Original version of this file comes from:
# https://github.com/googleworkspace/python-samples/blob/master/calendar/quickstart/quickstart.py
# With the following licensing. It was adjusted to fit this use and project.
#
# Copyright 2018 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from datetime import datetime, timedelta, timezone
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

from utils import hour_rounder

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/calendar.readonly']

def __auth():
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('calendar-token.pickle'):
        with open('calendar-token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'calendar-credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('calendar-token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    return build('calendar', 'v3', credentials=creds)

def __get_datetime(event, which):
    str_date = event[which].get('dateTime', event[which].get('date'))
    return datetime.fromisoformat(str_date)

def mocked_events(days):
    ret = []
    ret.append({'start_day': 0, 'start_hour': 13, 'duration': 3, 'summary': "Event 1"})
    ret.append({'start_day': 2, 'start_hour': 17, 'duration': 7, 'summary': "Event 2"})
    return ret

def events(days):
    '''
    Returns an array of events on the format:
        {"start_day": <int (week_day)>, "start_hour": <int>,
         "duration": <int (in hours)>, "summary": <string> }
    '''
    service = __auth()
    now = datetime.now(timezone.utc)
    now_iso = datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
    timeMax_iso = (datetime.utcnow() + timedelta(days=days)).isoformat() + 'Z'
    events_result = service.events().list(calendarId='primary', timeMin=now_iso,
                                          timeMax=timeMax_iso, singleEvents=True,
                                          orderBy='startTime').execute()
    events = events_result.get('items', [])

    if not events:
        return []
    ret = []
    for event in events:
        start = hour_rounder(__get_datetime(event, 'start'))
        end = hour_rounder(__get_datetime(event, 'end'))
        duration = int((end-start).seconds/(60*60))

        start_day = (start - now).days
        start_hour = start.hour

        summary = event['summary']
        ret.append({'start_day': start_day, 'start_hour': start_hour,
                    'duration': duration, 'summary': summary})
    return ret
