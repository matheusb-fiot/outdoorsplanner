#!/usr/bin/env python3

from flask import Flask, render_template, request
app = Flask(__name__)

import locale
locale.setlocale(locale.LC_ALL, 'pt_BR.utf8')
from calendar import day_name
from datetime import datetime, timedelta

import utils
import gcalendar
import forecast

def next_days_names(n):
    now = datetime.now()
    ret = []
    for i in range(n):
        ret.append(day_name[(now + timedelta(days=i)).weekday()])
    return ret

def make_empty_schedule():
    schedule = [{} for i in range(utils.VIEW_DAYS)]

    for name, day in zip(next_days_names(utils.VIEW_DAYS), schedule):
        day['name'] = name

    for day in schedule:
        day['events'] = ["" for i in range(24)]
        day['daylight'] = [True for i in range(24)]
        day['temps'] = [0 for i in range(24)]
        day['rain'] = [0 for i in range(24)]
        day['cloud'] = [0 for i in range(24)]

    return schedule

def add_gcalendar_events(schedule):
    for event in gcalendar.events(utils.VIEW_DAYS):
        duration = event['duration']
        day_idx = event['start_day']
        hour = event['start_hour']
        while duration > 0:
            schedule[day_idx]['events'][hour] += " '" + event['summary'] + "'"
            duration -= 1
            hour += 1
            if hour >= 24:
                day_idx += 1
                hour = hour % 24
                if day_idx >= 3:
                    break

def add_daylight_info(schedule):
    for day, astro in enumerate(forecast.sun_times()):
        daylight = schedule[day]['daylight']
        for i in range(astro['sunrise']):
            daylight[i] = False
        for i in range(astro['sunset'], 24):
            daylight[i] = False

def add_temp_info(schedule):
    for day, temps in enumerate(forecast.temp_per_day()):
        schedule[day]['temps'] = temps

def add_rain_info(schedule):
    for day, rain in enumerate(forecast.rain_chance_per_day()):
        schedule[day]['rain'] = rain

def add_cloud_info(schedule):
    for day, cloud in enumerate(forecast.cloud_coverage_per_day()):
        schedule[day]['cloud'] = cloud

def add_icons(schedule):
    for day, icons in enumerate(forecast.icons_per_day()):
        schedule[day]['icons'] = icons

def add_summary_text(schedule):
    for day, text in enumerate(forecast.summary_per_day()):
        schedule[day]['summary_text'] = text

def min_and_max_temp(schedule):
    mi = 300
    ma = -300
    for day in schedule:
        for temp in day['temps']:
            if temp < mi:
                mi = temp
            elif temp > ma:
                ma = temp
    return mi, ma

@app.route('/')
def root(name=None):

    alert = None
    info = None

    loc = request.args.get("location")
    if loc is None:
        loc_status = forecast.set_default_location()
    else:
        loc_status = forecast.set_location(loc)

    if loc_status == "failed":
        alert = ("Localização '%s' não encontrada. Utilizando '%s' no lugar." %
                 (loc, forecast.get_location()))
    elif loc_status == "changed":
        info = ("Localização modificada. Utilizando '%s'" % forecast.get_location())


    schedule = make_empty_schedule()
    add_gcalendar_events(schedule)
    add_daylight_info(schedule)
    add_temp_info(schedule)
    add_rain_info(schedule)
    add_cloud_info(schedule)
    add_icons(schedule)
    add_summary_text(schedule)
    min_temp, max_temp = min_and_max_temp(schedule)

    return render_template('index.html', alert=alert, info=info, schedule=schedule,
                           min_temp=min_temp, max_temp=max_temp,
                           hours=[ "%s:00" % str(h).zfill(2) for h in range(24) ],
                           current_hour=utils.hour_rounder(datetime.now()).hour,
                           weather_region=forecast.get_location())
