# Planejador de atividades ao ar livre

Sistema desenvolvido para a matéria "MAC6929 - Fundamentos da Internet das
Coisas", do IME-USP (2020).

Um sistema para planejamento de atividades ao ar livre, integrando dados de
tempo hora-a-hora com dados de compromissos do usuário, importados do Google
Agenda.

## Como executar

1. Configure seu acesso para a API do Google Calendar

	Habilite a API do Google Calendar e faça o download do arquivo de
	configurações do cliente, salvando-o em `calendar-credentials.json`.
	Para mais informações, veja:
	https://developers.google.com/calendar/quickstart/python.

2. Configure o acesso na Weather API

	O planejador utiliza os dados da https://www.weatherapi.com. Faça uma
	conta gratuita no site, gere uma chave e utilize-a no passo 4.

3. Instale as dependencias

	```
	$ pipenv install
	```

4. Rode o app
```
$ export FLASK_APP=main.py
$ export PLANNER_FORECAST_KEY=<SUA_CHAVE_DO_WEATHER_API>
$ export FLASK_ENV=development # opcional
$ pipenv run flask run
$ # Acesse o ip:porta informado no terminal em seu navegador
```

## Utilização

O flask ira informar em seu terminal, o endereço e porta onde o servidor
estará esperando requisições. Acesse este endereço em seu navegador. No
primeiro acesso, será pedido que você autorize o app para acessar seu Google
Calendar. Depois disso, o token será salvo em "calendar-token.pickle".

## Notas e limitações

- Apesar de rodar um servidor, o sistema foi feito para ser rodado localmente.
  Por exemplo, note que a autorização do Google é sempre pedida na maquina onde
  o servidor está rodando (logo, não há como ter >1 cliente).
- O sistema foi codificado pensando em uma visualização de até 3 dias. Essa
  limitação foi adotada pois as APIs de clima gratuitas geralmente só
  disponibilizam previsão hora a hora até 3 dias.
