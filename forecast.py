import locale
from datetime import datetime
import requests
import os
import geocoder

import utils

API_BASE = "http://api.weatherapi.com/v1"
API_FORECAST = "%s/forecast.json" % API_BASE
API_SEARCH = "%s/search.json" % API_BASE
API_KEY = os.environ['PLANNER_FORECAST_KEY']

__cached_weather_data = None
__cached_time = None

__cached_city_guess = None
def __guess_city():
    global __cached_city_guess
    if __cached_city_guess is not None:
        return __cached_city_guess
    city = geocoder.ip('me').city
    __cached_city_guess = city
    return city

location = None
location_name = "" 
location_changed = False # Whether user requested a location change

def __check_api_error(response):
    if 'error' in response:
        error = response['error']
        err_msg = error['message'] if 'message' in error else "unknown error"
        raise Exception("forecast API error: %s" % response['error']['message'])

def __search_location(loc_str):
    params = {'key': API_KEY, 'q': loc_str}
    response = requests.get(API_SEARCH, params=params).json()
    try:
        __check_api_error(response)
    except:
        return None
    if len(response) == 0:
        return None
    return response[0]

def __set_location(loc):
    global location
    global location_name
    print("Setting location: name='%s' lat='%s' lon='%s'" %
            (loc['name'], loc['lat'], loc['lon']))
    location = "%f,%f" % (loc['lat'], loc['lon'])
    location_name = loc['name']

def __check_location():
    if location is None:
        city = __guess_city()
        print("Searching '%s'" % city)
        response = __search_location(city)
        if response is None:
            fallback = "São Paulo"
            print("Failed. Searching '%s'" % fallback)
            response = __search_location(fallback)
            if response is None:
                raise Exception ("Internal error: can't find location for forecast API")
        __set_location(response)

def __refresh_cache():
    global __cached_weather_data
    global __cached_time
    global location_changed
    print("Refreshing forecast data")
    __check_location()
    params = {'key': API_KEY, 'q': location, 'days': utils.VIEW_DAYS, 'lang': 'pt'}
    response = requests.get(API_FORECAST, params=params).json()
    __check_api_error(response)
    __cached_weather_data = response
    __cached_time = datetime.now()
    location_changed = False

def _weather_data():
    if location_changed or __cached_time is None or ((datetime.now() - __cached_time).seconds >= 3600):
        __refresh_cache()
    return __cached_weather_data["forecast"]["forecastday"]

last_search = None

def set_location(search):
    global location_changed
    global last_search
    if search == last_search:
        return "unchanged" 
    response = __search_location(search)
    if response is None:
        return "failed"
    __set_location(response)
    location_changed = True
    last_search = search
    return "changed"

def set_default_location():
    global location
    global last_search
    if last_search is None:
        return "unchanged"
    location = None
    last_search = None
    __check_location()
    return "changed"
    # No "failed", as __check_location() will raise an exception on error

def get_location():
    return location_name

def sun_times():
    astros = []
    locale.setlocale(locale.LC_ALL, 'en_US.utf8')
    for day in _weather_data():
        sunrise = utils.hour_rounder(datetime.strptime(day["astro"]["sunrise"], "%I:%M %p")).hour
        sunset = utils.hour_rounder(datetime.strptime(day["astro"]["sunset"], "%I:%M %p")).hour
        astros.append({ "sunrise": sunrise, "sunset": sunset })
    locale.setlocale(locale.LC_ALL, 'pt_BR.utf8')
    return astros

def temp_per_day():
    temps = []
    for day in _weather_data():
        temps.append([ hour["temp_c"] for hour in day["hour"] ])
    return temps

def rain_chance_per_day():
    rain = []
    for day in _weather_data():
        rain.append([ int(hour["chance_of_rain"]) for hour in day["hour"] ])
    return rain

def cloud_coverage_per_day():
    cloud = []
    for day in _weather_data():
        cloud.append([ int(hour["cloud"]) for hour in day["hour"] ])
    return cloud

def icons_per_day():
    icons = []
    for day in _weather_data():
        icons.append([ hour["condition"]["icon"] for hour in day["hour"] ])
    return icons

def summary_per_day():
    summary = []
    for day in _weather_data():
        summary.append([ hour["condition"]["text"] for hour in day["hour"] ])
    return summary
