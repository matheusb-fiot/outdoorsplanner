function forAllInClass(cls, func)
{
	elements = document.getElementsByClassName(cls);
	for (i = 0; i < elements.length; i++) {
		func(elements[i]) 
	}
}

function displayNone(el) { el.style.display = "none" }
function displayInline(el) { el.style.display = "inline" }

function indicators(opt)
{
	forAllInClass("fa-moon",
		function(el) { opt.value == "night" ? displayInline(el) : displayNone(el) });
	forAllInClass("summary-icon",
		function(el) { opt.value == "summary-icon" ? displayInline(el) : displayNone(el) });
}

function colormap(opt)
{
	if (opt.value != "summary-text") {
		elements = document.getElementsByClassName('hour-box');
		for (i = 0; i < elements.length; i++) {
			el = elements[i];

			if (el.getAttribute("data-has-event") == "True")
				continue;

			if (opt.value == "temp")
				color = `rgba(255, 123, 123, ${el.getAttribute("data-temp-opacity")})`;
			else if (opt.value == "rain")
				color = `rgba(130, 186, 255, ${el.getAttribute("data-rain-opacity")})`;
			else if (opt.value == "cloud")
				color = `rgba(180, 180, 220, ${el.getAttribute("data-cloud-opacity")})`;
			else
				color = "";

			el.style.backgroundColor = color;
		}
	}

	forAllInClass("temp", displayNone);
	forAllInClass("cloud", displayNone);
	forAllInClass("rain", displayNone);
	forAllInClass("summary-text", displayNone);
	forAllInClass(opt.value, displayInline);
}

function location_change()
{
	value = document.getElementById("location_text").value;
	window.location.search = `?location=${value}`;
}

function location_text_keychange()
{
	if (event.keyCode == 13)
		document.getElementById("location_btn").click()
}

/* Refresh checkbox/radios on page refresh */
function startup()
{
	elements = document.querySelectorAll('[checked]');
	for (i = 0; i < elements.length; i++) {
		elements[i].checked = true;
	}
}


startup();
